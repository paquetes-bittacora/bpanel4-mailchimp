# bPanel4 Mailchimp

Paquete para mostrar un formulario de suscripción a una newsletter de Mailchimp en bPanel4.

## Instalación

El paquete se instala automáticamente, pero es necesario completar los siguientes valores en el `.env`:

```
NEWSLETTER_DRIVER="Spatie\Newsletter\Drivers\MailChimpDriver::class"
NEWSLETTER_API_KEY= # Se obtiene en la cuenta de MailChimp
NEWSLETTER_LIST_ID= # Se obtiene en la cuenta de MailChimp
NEWSLETTER_ENDPOINT=null # Este valor debe estar vacío 
```

## Uso

**Nota**: Según los issues del paquete de Spatie en el que se basa este, es posible que en local no funcione, le
pasa a unos proyectos si y a otros no, así que hay que probar en un servidor de prueba.

Para incluir el formulario en la página, hay que llamar a:

```
@include('bpanel4-mailchimp::form', ['policyText' => 'Acepto las condiciones legales'])
```

Viene sin estilo, pero con varias clases puestas para facilitar cambiar la apariencia del componente.
