<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\MailChimp\Tests\Acceptance;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class InstallCommandTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        touch('.env.testing');
        copy(base_path() . '/.env.testing', base_path() . '/.env.testing.bk');
    }

    public function testElComandoDeInstalacionSeEjecuta(): void
    {
        $result = $this->artisan('bpanel4-mailchimp:install');
        $result->assertOk();
    }

    public function testAnadeLaConfigurarionDeMailchimp(): void
    {
        $this->artisan('bpanel4-mailchimp:install');
        $contents = file_get_contents(base_path() . '/.env.testing');
        self::assertStringContainsString('MailChimp', $contents);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        unlink(base_path() . '/.env.testing');
        copy(base_path() . '/.env.testing.bk', base_path() . '/.env.testing');
        unlink(base_path() . '/.env.testing.bk');
    }
}
