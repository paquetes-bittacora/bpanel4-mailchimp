<?php

declare(strict_types=1);

use Bittacora\Bpanel4\MailChimp\Http\Controllers\MailchimpPublicController;
use Illuminate\Support\Facades\Route;

Route::prefix('/mailchimp')->name('bpanel4-mailchimp.')->middleware(['web'])
    ->group(static function () {
        Route::post('/suscribir', [MailchimpPublicController::class, 'subscribe'])->name('subscribe');
    });
