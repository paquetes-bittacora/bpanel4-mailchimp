<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\MailChimp\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class InstallCommand extends Command
{
    /** @var string */
    protected $signature = 'bpanel4-mailchimp:install';

    /** @var string */
    protected $description = 'Instala el paquete de suscripción a newsletters de MailChimp';

    private const PERMISSIONS = ['edit', 'update'];

    public function handle(AdminMenu $adminMenu): void
    {
        $this->registerEnvVariables();
    }

    private function registerEnvVariables(): void
    {
        $env = App::environment();
        $file = in_array($env, ['production', 'local']) ? base_path() . '/.env' : base_path() . '/.env.' . $env;

        $fileContents = file_get_contents($file);

        // El siguiente bloque hay que dejarlo mal formateado para que salga bien en el .env
        $string = "\n# bPanel4 Mailchimp ------------------------------
NEWSLETTER_DRIVER=\"Spatie\\\\Newsletter\\\\Drivers\\\\MailChimpDriver\"\n
NEWSLETTER_API_KEY= # Se obtiene en la cuenta de MailChimp
NEWSLETTER_LIST_ID= # Se obtiene en la cuenta de MailChimp
NEWSLETTER_ENDPOINT=null
# /bPanel4 Mailchimp -----------------------------";

        if (!str_contains($fileContents, 'bPanel4 Mailchimp')) {
            file_put_contents($file, $string, FILE_APPEND);
        }
    }
}
