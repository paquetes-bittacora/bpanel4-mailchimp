<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\MailChimp\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Spatie\Newsletter\Facades\Newsletter;
use Illuminate\Routing\Redirector;

final class MailchimpPublicController
{
    public function __construct(
        private readonly Redirector $redirector,
    ) {
    }

    public function subscribe(Request $request): RedirectResponse
    {
        if (Newsletter::subscribeOrUpdate($request->get('email'))) {
            return $this->redirector->back()->with(['alert-success' => '¡Gracias por suscribirse a nuestra newsletter!']);
        }

        Newsletter::getApi()->getLastError();
        Newsletter::getApi()->getLastResponse();
        return $this->redirector->back()->with(['alert-danger' => 'Ocurrió un error al suscribirle a la newsletter']);
    }
}
